import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  public name = 'John Doe';
  public popupVisible = true;

  constructor() {
    setInterval(() => {
      this.name = this.name + 'E';
    }, 3000);
  }

}
