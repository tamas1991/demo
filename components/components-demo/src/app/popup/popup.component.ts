import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent {

  @Input() visible: boolean;
  @Output() visibleChange: EventEmitter<boolean>;

  constructor() {
    this.visibleChange = new EventEmitter<boolean>();
  }

  public close(): void {
    this.visibleChange.emit(false);
  }

}
