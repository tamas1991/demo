import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {MultiSelectModule} from 'primeng/multiselect';

import { AppComponent } from './app.component';
import { TemplateDrivenFormComponent } from './forms/template-driven-form/template-driven-form.component';
import { ReactiveFormComponent } from './forms/reactive-form/reactive-form.component';
import { AppRoutingModule } from './app-routing.module';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MinSelectionCountDirective } from './forms/validators/min-selection-count.directive';
import { ErrorDisplayerComponent } from './forms/error-displayer/error-displayer.component';
import { RouteReuseStrategy } from '@angular/router';
import { CacheRouteReuseStrategy } from './route-strategy/cache-route-reuse-strategy';

@NgModule({
  declarations: [
    AppComponent,
    TemplateDrivenFormComponent,
    ReactiveFormComponent,
    MinSelectionCountDirective,
    ErrorDisplayerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    InputTextModule,
    MultiSelectModule,
    BrowserAnimationsModule,
    ButtonModule,
    MessagesModule,
    MessageModule
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    // {provide: RouteReuseStrategy, useClass: CacheRouteReuseStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
