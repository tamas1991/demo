import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import { Vo } from '../vo';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent {

  public formModel: FormGroup;
  public vo: Vo;

  public cityOptions = [
    {name: 'Budapest', id: 0},
    {name: 'Szolnok', id: 1},
    {name: 'Debrecen', id: 20},
    {name: 'Miskolc', id: 56},
    {name: 'Sopron', id: 9994},
  ];

  constructor(private formBuilder: FormBuilder) {
    this.vo = new Vo();
    this.initForm();
    // this.initFormWithFormBuilder();
  }

  private initForm(): void {
    this.formModel = new FormGroup({
      givenName: new FormControl('Initial given name', [Validators.required, Validators.maxLength(20)]),
      familyName: new FormControl('Initial family name', [Validators.required, Validators.minLength(6)]),
      cities: new FormControl([this.cityOptions[0]], [Validators.required, minSelectionCount(3)])
    });
  }

  private initFormWithFormBuilder(): void {
    this.formModel = this.formBuilder.group({
      givenName: ['Initial given name', [Validators.required, Validators.maxLength(20)]],
      familyName: ['Initial family name',[Validators.required, Validators.minLength(6)]],
      cities: [[this.cityOptions[0]], [Validators.required, minSelectionCount(3)]]
    });
  }

  public updateValues(): void {
    const vo = new Vo();
    vo.familyName = 'Updated family name';
    vo.givenName = 'Updated given name';
    vo.cities = [];
    this.formModel.setValue(vo);
  }

}

function minSelectionCount(count: number): (control: AbstractControl) => ValidationErrors | null {
  return function(control: AbstractControl): ValidationErrors | null {
    if (isNullOrUndefined(control.value) || control.value.length < count) {
      return {minSelectionCount: true};
    }
    return null;
  };
}

