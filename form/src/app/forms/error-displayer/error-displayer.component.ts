import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-error-displayer',
  templateUrl: './error-displayer.component.html',
  styleUrls: ['./error-displayer.component.scss']
})
export class ErrorDisplayerComponent implements OnInit {

  @Input()
  public control: AbstractControl;

  public message: any[];

  constructor() { }

  ngOnInit() {
    console.log('--- this.control ---\n', this.control);
    this.control.statusChanges.subscribe(newStatus => {
      console.log('--- newStatus ---\n', newStatus);
      this.updateMessage();
    })
  }

  private updateMessage(): void {
    console.log('--- this.control.errors ---\n', this.control.errors);
    if (isNullOrUndefined(this.control.errors)) {
      this.message = null;
      return;
    }

    Object.keys(this.control.errors).forEach(errorName => {
      console.log('errorName: ' + errorName);
      console.log('--- this.control.errors[errorName] ---\n', this.control.errors[errorName]);
      switch(errorName) {
        case 'required':
          this.message = [{detail: 'A mező kitöltése kötelező', severity: 'error'}];
          break;
        case 'minlength':
          this.message = [{detail: 'Nem elég hosszú a szöveg', severity: 'error'}];
          break;
        case 'maxlength':
          this.message = [{detail: 'Túl hosszú a szöveg', severity: 'error'}];
          break;
        case 'minSelectionCount':
          this.message = [{detail: 'Válasszon ki többet', severity: 'error'}];
          break;
        case 'asyncValidatoinError':
          this.message = [{detail: 'Aszinkron validációs hiba', severity: 'error'}];
          break;
        default:
          this.message = [{detail: 'Ismeretlen hiba', severity: 'error'}];
          break;
      }
    })
  }

}
