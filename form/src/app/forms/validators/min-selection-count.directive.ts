import { Directive, Input, forwardRef } from '@angular/core';
import { Validator, AbstractControl, ValidationErrors, NG_VALIDATORS } from '@angular/forms';
import { isNullOrUndefined } from 'util';

@Directive({
  selector: 'p-multiSelect[appMinSelectionCount]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => MinSelectionCountDirective), multi: true }
  ]
})
export class MinSelectionCountDirective implements Validator {

  @Input()
  public appMinSelectionCount: number;

  public validate(control: AbstractControl): ValidationErrors | null {
    if (isNullOrUndefined(control.value) || control.value.length < this.appMinSelectionCount) {
      return {minSelectionCount: true};
    }
    return null;
  }

}
