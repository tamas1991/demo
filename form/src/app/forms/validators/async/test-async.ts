import { AbstractControl, ValidationErrors } from "@angular/forms";

export function testAsync(control: AbstractControl): Promise<ValidationErrors | null> {
    console.log('test async called');
    return new Promise<ValidationErrors | null>(resolve => {
        setTimeout(() => {
            const result = control.value.length < 10 ? null : {asyncValidatoinError: true};
            console.log('--- result ---\n', result);
            resolve(result);
        }, 3000);
    });

}