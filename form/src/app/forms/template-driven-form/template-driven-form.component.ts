import { Component } from '@angular/core';
import { Vo } from '../vo';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.scss']
})
export class TemplateDrivenFormComponent {

  public vo: Vo;

  public cityOptions = [
    {name: 'Budapest', id: 0},
    {name: 'Szolnok', id: 1},
    {name: 'Debrecen', id: 20},
    {name: 'Miskolc', id: 56},
    {name: 'Sopron', id: 9994},
  ];

  constructor() {
    this.vo = new Vo();
    this.vo.givenName = 'Initial given name';
    this.vo.familyName = 'Initial family name';
    this.vo.cities = [this.cityOptions[0]];
  }

  public updateValues(): void {
    this.vo.givenName = 'Updated given name';
    this.vo.familyName = 'Updated family name';
    this.vo.cities = [];
  }

}
