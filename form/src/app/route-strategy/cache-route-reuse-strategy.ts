import { RouteReuseStrategy, DetachedRouteHandle, ActivatedRouteSnapshot } from "@angular/router";

export class CacheRouteReuseStrategy implements RouteReuseStrategy {

    /** Minden alkalommal hívódik, amikor route-ok között navigálunk.
     * Theát: ha igaz, vagyis "should reuse route", akkor a mostani url-en maradunk, "újrahasználjuk" azt, ahol vagyunk.
     * @future az a route, amit elhagyunk
     * @curr az a route, amire megyünk
     * 
     * @return
     * true - a routing nem fog megtörténni
     * false - a routing meg fog történni
     */
    shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
        // console.log('shouldReuseRoute');
        // console.log('--- future ---\n', future);
        // console.log('--- curr ---\n', curr);
        return false;        // elvileg így sosem navigálunk
    }

    shouldAttach(route: ActivatedRouteSnapshot): boolean {
        console.log('should attach');
        return false;
    }

    shouldDetach(route: ActivatedRouteSnapshot): boolean {
        console.log('should detach');
        return false;
    }
    store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
        console.log('store');
    }
    retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
        console.log('retrive\n', route);
        return null;
    }



}