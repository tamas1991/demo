// 1. Üres konstruktor defaultból van.
// 2. Az instanceof használható.
class EmptyClass {
}
var emptyClassInstance = new EmptyClass();
console.log('emptyClassInstance instanceof EmptyClass: ' + (emptyClassInstance instanceof EmptyClass));

// Csak egy példa a constructor -ra
class ConstructorClass {
	constructor() {
		console.log('ConstructorClass created.');
	}
}
var constructorClassInstance = new ConstructorClass();

// 1. Ugyanolyan nével létrehozhatunk metódusokat, le fog fordulni.
// 2. Az utolsóként létrehozott felülvágja az előzőeket.
class MethodOverloadClass {

	print(param) {
		console.log('param: ' + param);
	}
	
	print(...params) {
		params.forEach(element => console.log('element: ' + element));
	}
	
	print(param1, param2) {
		console.log('param1: ' + param1 + '  param2: ' + param2);
	}

}
var overloadTest = new MethodOverloadClass();
overloadTest.print('only param');
console.log('--- MethodOverloadClass.prototype ---\n', MethodOverloadClass.prototype);

// 1. Létezik öröklődés class-ok között.
// 2. A contructor -ban kötelező, hogy a super hívást kiírjuk az első lépésként.
// 3. A class-on belül metódus és property hivatkozásoknál kötelező a this használata.
// 4. Nincs többszörös öröklődés.
class Shape {
    constructor (id, x, y) {
        this.id = id
        this.move(x, y)
    }
    move (x, y) {
        this.x = x
        this.y = y
    }
}

class Circle extends Shape {
    constructor (id, x, y, radius) {
        super(id, x, y)
        this.radius = radius
    }
}


class ClassWithGettersAndSetters {
	
	get name() {
		console.log('name getter called');
		return this._name;
	}
	set name(newName) {
		console.log('name setter called');
		this._name = newName;
	}
	
	get age() {
		console.log('age getter called');
		return this._age;
	}
	set age(newAge) {
		console.log('age setter called');
		this._age = newAge;
	}

}
var instanceWithGettersAndSetters = new ClassWithGettersAndSetters();
instanceWithGettersAndSetters.name = 'Van Góg';
instanceWithGettersAndSetters.age = 26;
console.log('name: ' + instanceWithGettersAndSetters.name);
console.log('age: ' + instanceWithGettersAndSetters.age);

// A getter/setter párost nem lehet metódusként használni
instanceWithGettersAndSetters.name();

