// Időzített function
console.log('Starting timeout...');
setTimeout(() => console.log('timed fn executed'), 2000);

// Időzített function megállítása még a végrehajtás előtt
var timeoutId = setTimeout(() => console.log('timed fn executed'), 2000);
clearTimeout(timeoutId);

// Ismétlődő function elindítása és megállítása
console.log('Starting interval...');
var counter = 1;
var intervalId = setInterval(() => console.log('interval counter: ' + counter++), 3000);    // Az első végrehajtás előtt is megvárja a megadott időt
setTimeout(() => {
    clearInterval(intervalId);
    console.log('Interval stopped');
}, 9000);

// Egy lehetséges használati eset - autocomplete keresés késleltetése
var timeoutId;
function handleAutocompleteSearch(searchString) {
    console.log('handleAutocompleteSearch called with searchString: ' + searchString);
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => callBackend(searchString), 500);
}

function callBackend(searchString) {
    console.log('backend called with searchString: ' + searchString);
}

setTimeout(() => handleAutocompleteSearch('B'), 200);
setTimeout(() => handleAutocompleteSearch('Bu'), 400);
setTimeout(() => handleAutocompleteSearch('Bud'), 600);
setTimeout(() => handleAutocompleteSearch('Buda'), 800);
setTimeout(() => handleAutocompleteSearch('Budap'), 900);
setTimeout(() => handleAutocompleteSearch('Budape'), 1000);
setTimeout(() => handleAutocompleteSearch('Budapes'), 1100);

// Végrahajtás hátratolása
function fn() {
    console.log('before timeout');
    setTimeout(() => console.log('timeout log with 0 delay'), 0);
    console.log('after timeout');
    fn2();
}
function fn2() {
    console.log('fn 2 called');
}
fn();

var num;
var limit = 999999999;
function loopFn() {
    num = -100000;
    while (num++ < limit) {}
    console.log('while finished');
}
loopFn();

setTimeout(() => {
    console.log('timout called');
    num = limit;
}, 0)
loopFn();







