// Hagyományos és arrow function
const normalFn = function () {console.log('normal function');};
const arrowFn = () => console.log('arrow function');

// Param syntax
const arrow1Fn = param => console.log('param: ' + param);
const arrow2Fn = (param1, param2) => console.log('param1: ' + param1 + ', param2: ' + param2);

// Body syntax
const arrowBody1Fn = num => {
	return num * 2;
};
const arrowBody2Fn = num => num * 2;                             // egy paraméter esetében elhagyható a zárójelpár, de nem kötelező elhagyni
const arrowBody3Fn = num => ({name: 'arrowBody3Fn', num: num});  // névtelen objektum visszaadása


// Mi a this?
function hostFunction() {
	const hostObject = {
		normalFn: function () {console.log('--- normal fn this ---\n', this);},
		arrowFn: () => console.log('--- arrow fn this ---\n', this)
	};
	
	hostObject.normalFn();
	hostObject.arrowFn();
	console.log('--- host fn this ---\n', this);
}
hostFunction();

