"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var instanceCounter = 0;
function CountInstances(constructorFn) {
    return (/** @class */ (function (_super) {
        __extends(class_1, _super);
        function class_1() {
            var _this = _super.call(this, arguments) || this;
            console.log("Created instances: " + ++instanceCounter);
            return _this;
        }
        return class_1;
    }(constructorFn)));
}
exports.CountInstances = CountInstances;
function Component(configObj) {
    return function (constructorFn) {
        return (/** @class */ (function (_super) {
            __extends(class_2, _super);
            function class_2() {
                var _this = _super.call(this, arguments) || this;
                _this.componentMetadata = ComponentConfig.fromRawObject(configObj);
                return _this;
            }
            return class_2;
        }(constructorFn)));
    };
}
exports.Component = Component;
var ComponentConfig = /** @class */ (function () {
    function ComponentConfig(selector, templateUrl, styleUrls) {
        this.selector = selector;
        this.templateUrl = templateUrl;
        this.styleUrls = styleUrls;
    }
    ComponentConfig.fromRawObject = function (rawObject) {
        return new ComponentConfig(rawObject.selector, rawObject.templateUrl, rawObject.styleUrls);
    };
    return ComponentConfig;
}());
exports.ComponentConfig = ComponentConfig;
//# sourceMappingURL=class-decorators.js.map