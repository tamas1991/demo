"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var static_utils_1 = require("./static-utils");
/**
 * Puts a log before and after a function call
 * @param target Either the constructor function of the class for a static member or the prototype of the class for an instance member.
 * @param key The name of the member.
 * @param descriptor The Property Descriptor for the member.
 */
function TraceLog(target, key, descriptor) {
    var originalMethod = descriptor.value;
    descriptor.value = function () {
        console.log("Start method: " + key);
        originalMethod.apply(this, arguments);
        console.log("End method: " + key);
    };
}
exports.TraceLog = TraceLog;
function Protected(target, key, descriptor) {
    var originalMethod = descriptor.value;
    descriptor.value = function () {
        if (!static_utils_1.StaticUtils.authenticated) {
            console.error('You must be authenticated to use this method!');
            return;
        }
        originalMethod.apply(this, arguments);
    };
}
exports.Protected = Protected;
function DecoratorFactory(configObject) {
    var _this = this;
    return function (target, key, descriptor) {
        if (configObject.protection) {
            _this.Protected(target, key, descriptor);
        }
        if (configObject.trace) {
            _this.TraceLog(target, key, descriptor);
        }
    };
}
exports.DecoratorFactory = DecoratorFactory;
function Override(objectPrototype, methodName, descriptor) {
    var superClassPrototype = objectPrototype.__proto__;
    var superMethodFound = false;
    while (Object.keys(superClassPrototype).length > 0) { // While not empty object
        if (typeof superClassPrototype[methodName] !== 'undefined' && superClassPrototype[methodName] !== null) { // Maszek isNullOrUndefined
            superMethodFound = true;
            break;
        }
        superClassPrototype = superClassPrototype.__proto__;
    }
    if (!superMethodFound) {
        throw new Error("The method \"" + methodName + "\" is marked by @Override but it doesn't actually override a superclass method!");
    }
}
exports.Override = Override;
//# sourceMappingURL=method-decorators.js.map