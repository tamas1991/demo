"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function LogChanges(target, key) {
    // original value
    var value = target[key];
    // getter and setter
    var getter = function () { return value; };
    var setter = function (newValue) {
        value = newValue;
        console.log("new value of " + key + ": " + newValue);
    };
    // replace property with the getter and setter
    if (delete target[key]) {
        Object.defineProperty(target, key, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
    }
}
exports.LogChanges = LogChanges;
//# sourceMappingURL=property-decorators.js.map