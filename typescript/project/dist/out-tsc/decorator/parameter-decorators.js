"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getMetadataFieldNameFor(methodName) {
    return "_required_argument_indices_for_" + methodName;
}
function Required(target, key, index) {
    var metadataFieldName = getMetadataFieldNameFor(key);
    if (!target[metadataFieldName]) {
        target[metadataFieldName] = [];
    }
    var requiredIndices = target[metadataFieldName];
    requiredIndices.push(index);
}
exports.Required = Required;
function ValidateArgs(target, key, descriptor) {
    var metadataFieldName = getMetadataFieldNameFor(key);
    var requiredIndices = target[metadataFieldName];
    if (!requiredIndices) {
        return;
    }
    var originalMethod = descriptor.value;
    descriptor.value = function () {
        var originalArgs = arguments;
        requiredIndices.forEach(function (requiredArgIndex) {
            var argValue = originalArgs[requiredArgIndex];
            if (typeof argValue === 'undefined' || argValue === null) {
                throw new Error("You gave \"" + argValue + "\" to method \"" + key + "\" in place of a required parameter! ");
            }
        });
        originalMethod(arguments);
    };
}
exports.ValidateArgs = ValidateArgs;
//# sourceMappingURL=parameter-decorators.js.map