"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var method_decorators_1 = require("./method-decorators");
var parameter_decorators_1 = require("./parameter-decorators");
var VerySuper = /** @class */ (function () {
    function VerySuper() {
    }
    // public overrideTest(): void {
    // }
    VerySuper.prototype.onlyVerySuperMethod = function () {
    };
    return VerySuper;
}());
var SomeOtherClass = /** @class */ (function (_super) {
    __extends(SomeOtherClass, _super);
    function SomeOtherClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SomeOtherClass;
}(VerySuper));
// @CountInstances
// @Component({
//     selector: 'app-my-component',
//     templateUrl: './my-template.html',
//     styleUrls: ['./my-style1.scss', './my-style2.scss']
// })
var DecoratorTests = /** @class */ (function (_super) {
    __extends(DecoratorTests, _super);
    function DecoratorTests() {
        var _this = _super.call(this) || this;
        // @LogChanges
        _this.publicField = 'initial value, before everything';
        // @LogChanges
        _this.privateField = 22;
        // this.publicField = 'first value, in constructor';
        // this.privateField = 5987;
        // this.traceLogTest('param-1', 12);
        // console.log('-- before auth --');
        // this.protectionTest();
        // this.testBoth();
        // StaticUtils.authenticated = true;
        // console.log('-- after auth --');
        // this.protectionTest();
        // this.testBoth();
        // this.factoryTest();
        // this.methodWithParams('first param', 33, {id: 10, name: 'Name'});
        // setTimeout(() => {
        // Ha csak this -t logolom ki, akkor ezeket a metaadatokat nem fogja kiírni!!!
        // Megy timout nélkül is!!!
        console.log('--- this[....] ---\n', _this["_required_argument_indices_for_methodWithParams"]);
        return _this;
        // }, 2000);
        // this.overrideTest();
    }
    DecoratorTests.prototype.traceLogTest = function (param1, param2) {
        console.log("--- trace log test, param1: " + param1 + ", param2: " + param2);
    };
    DecoratorTests.prototype.protectionTest = function () {
        console.log('--- protected method called ---');
    };
    // Számít a sorrend!!
    DecoratorTests.prototype.testBoth = function () {
        console.log('--- method with both decorators called ---');
    };
    DecoratorTests.prototype.factoryTest = function () {
        console.log('--- factory test method called ---');
    };
    DecoratorTests.prototype.methodWithParams = function (param1, param2, param3) {
        console.log('methodWithParams called');
    };
    DecoratorTests.prototype.overrideTest = function () {
    };
    __decorate([
        method_decorators_1.TraceLog,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", void 0)
    ], DecoratorTests.prototype, "traceLogTest", null);
    __decorate([
        method_decorators_1.Protected,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], DecoratorTests.prototype, "protectionTest", null);
    __decorate([
        method_decorators_1.Protected,
        method_decorators_1.TraceLog,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], DecoratorTests.prototype, "testBoth", null);
    __decorate([
        method_decorators_1.DecoratorFactory({
            protection: true,
            trace: true
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], DecoratorTests.prototype, "factoryTest", null);
    __decorate([
        parameter_decorators_1.ValidateArgs,
        __param(0, parameter_decorators_1.Required), __param(1, parameter_decorators_1.Required), __param(2, parameter_decorators_1.Required),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Number, Object]),
        __metadata("design:returntype", void 0)
    ], DecoratorTests.prototype, "methodWithParams", null);
    __decorate([
        method_decorators_1.Override,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], DecoratorTests.prototype, "overrideTest", null);
    return DecoratorTests;
}(SomeOtherClass));
exports.DecoratorTests = DecoratorTests;
var SubClass = /** @class */ (function (_super) {
    __extends(SubClass, _super);
    function SubClass() {
        var _this = _super.call(this) || this;
        console.log('subclass created');
        return _this;
    }
    return SubClass;
}(DecoratorTests));
exports.SubClass = SubClass;
//# sourceMappingURL=decorator-tests.js.map