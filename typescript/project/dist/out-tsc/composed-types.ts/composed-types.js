"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function mergeObjects(primaryObj, secondaryObj) {
    var result = {};
    Object.assign(result, primaryObj);
    Object.assign(result, secondaryObj);
    return result;
}
function intersectionDemo() {
    var colored = { color: 'blue', common: 'colored common' };
    var identified = { id: 12, common: 'identified common' };
    var both = mergeObjects(colored, identified);
    //    const both: Both = mergeObjects(colored, identified);
    both.id;
    both.color;
}
function unionDemo() {
    var obj = { 0: 'value 0', 1: 'vlaue 1', 2: 'value 2' };
    var element0 = accessElement(obj, 0);
    var element2 = accessElement(obj, 'aa');
    console.log("element 0: " + element0);
    console.log("element 2: " + element2);
}
exports.unionDemo = unionDemo;
function accessElement(obj, key) {
    // const index = stringKeyToIndex(key);
    var index = typeof key === 'string' ? stringKeyToIndex(key) : key;
    return obj[index];
}
function stringKeyToIndex(key) {
    var index;
    // some very difficult calculation, decode hash...
    index = key.length;
    return index;
}
//# sourceMappingURL=composed-types.js.map