var MyClass = /** @class */ (function () {
    function MyClass() {
        this.readonlyField = { isReadonly: true };
    }
    MyClass.creatorFn = function () {
        var instance = new MyClass();
        instance.numField = 23;
        instance.strField = 'str';
        // instance.readonlyField = {};     // Error
        return instance;
    };
    MyClass.prototype.defaultPublicMethod = function (param) {
        // return 23;       // Error
        return function () {
            console.log('returned function called');
        };
    };
    MyClass.prototype.returnFunctionWithDetailedTypes = function () {
        return function (param_1, param_2) {
            console.log('param_1: ' + param_1 + '  param_2: ' + JSON.stringify(param_2));
        };
        // Így is lehet:
        // return function (param_1: string, param_2: number) {
        //     console.log('param_1: ' + param_1 + '  param_2: ' + JSON.stringify(param_2));
        // }
    };
    return MyClass;
}());
function myFunction() {
    // const myClassInstance = new MyClass();       // Error
    var myClassInstance = MyClass.creatorFn();
    var typedFn = myClassInstance.returnFunctionWithDetailedTypes();
    // typedFn('12', '25');     // Error
    typedFn('12', 25);
    // return 12;       // Error
}
//# sourceMappingURL=ts-syntax.js.map