"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var simple_map_1 = require("./simple-map");
var GenericsDemo = /** @class */ (function () {
    function GenericsDemo() {
        this.mapDemo();
    }
    GenericsDemo.prototype.mapDemo = function () {
        var map = new simple_map_1.SimpleMap();
        map.put('some key 1', [1, 9, 33, 678, 34.567, 33.102]);
        map.put('some key 2', [1, 9,]);
        map.put('some key 3', [33.102]);
        map.put('some key 4', []);
        // map.put(32432, [2,5,9]);
        console.log('some key 1: ' + map.get('some key 1'));
        simple_map_1.SimpleMap.logSimpleMap(map);
        var objects = [
            { id: 1, name: 'Name 1', text: 'Text 1' },
            { id: 2, name: 'Name 2' },
            { id: 3, text: 'Text 1' },
            { id: 4, name: 'Name 4', text: 'Text 1', extra: 'Extra' },
            { id: 5, name: 'Name 5', text: 'Text 1' },
            { id: 6, name: 'Name 6' },
            { id: 7, text: 'Text 1' },
        ];
        var mapFromObjects = simple_map_1.SimpleMap.createFrom(objects, 'id');
        console.log('------------------------------------');
        simple_map_1.SimpleMap.logSimpleMap(mapFromObjects);
    };
    return GenericsDemo;
}());
exports.GenericsDemo = GenericsDemo;
//# sourceMappingURL=generics-demo.js.map