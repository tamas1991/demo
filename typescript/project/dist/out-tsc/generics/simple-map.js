"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SimpleMap = /** @class */ (function () {
    function SimpleMap() {
        this.entries = [];
    }
    SimpleMap.createFrom = function (elements, keyField) {
        var newSimpleMap = new SimpleMap();
        elements
            .map(function (currentElement) { return ({ key: currentElement[keyField], value: currentElement }); })
            .forEach(function (entry) { return newSimpleMap.put(entry.key, entry.value); });
        return newSimpleMap;
    };
    SimpleMap.logSimpleMap = function (mapToLog) {
        mapToLog.entryset().forEach(function (entry) { return console.log("key: " + entry.key + ", value: " + JSON.stringify(entry.value)); });
    };
    SimpleMap.prototype.put = function (key, value) {
        var alreadyContainedObject = this.entries.find(function (entry) { return entry.key === key; });
        if (typeof alreadyContainedObject !== 'undefined' && alreadyContainedObject !== null) {
            alreadyContainedObject.value = value;
        }
        else {
            this.entries.push({ key: key, value: value });
        }
    };
    SimpleMap.prototype.get = function (key) {
        var entry = this.entries.find(function (entry) { return entry.key === key; });
        return typeof entry === 'undefined' || entry === null ? null : entry.value;
    };
    SimpleMap.prototype.entryset = function () {
        return new Set(this.entries);
    };
    return SimpleMap;
}());
exports.SimpleMap = SimpleMap;
//# sourceMappingURL=simple-map.js.map