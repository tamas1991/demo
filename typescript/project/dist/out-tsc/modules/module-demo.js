"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var importedMembers = require("./module01");
function moduleTestDemo() {
    console.log('--- importedMembers ---', importedMembers);
    importedMembers.moduleFn();
    new importedMembers.ModuleClass();
}
exports.moduleTestDemo = moduleTestDemo;
//# sourceMappingURL=module-demo.js.map