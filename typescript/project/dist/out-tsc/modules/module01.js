"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function moduleFn() {
    console.log('moduleFn called');
}
exports.moduleFn = moduleFn;
function innerFn() {
}
var ModuleClass = /** @class */ (function () {
    function ModuleClass() {
        console.log('ModuleClass created');
    }
    return ModuleClass;
}());
exports.ModuleClass = ModuleClass;
var InnerClass = /** @class */ (function () {
    function InnerClass() {
    }
    return InnerClass;
}());
exports.CONSTANTS = { CONST1: 'CONST1', CONST2: 'CONST2' };
//# sourceMappingURL=module01.js.map