"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function indexableDemo() {
    var numIndexable = { 0: 'Value 1', 1: 'Value 2' /*'strKey': 'Value 3*/ };
    var strIndexable = { 'key0': 'Value 1', 'key1': 'Value 2', 3: 'Value 3' };
    var bothIndexable = { 'key0': 'Value 1', 'key1': 'Value 2', 3: 'Value 3' };
    var notIndexable = { onlyParam: 'value', };
}
function usefulToStringDemo() {
    var strIndexable = { 'key0': 'Value 1', 'key1': 'Value 2', 3: 'Value 3' };
    console.log('--- strIndexable before override ---\n' + strIndexable);
    // Replace the default toString method on Object.prototype
    Object.prototype.toString = function () { return JSON.stringify(this); };
    console.log('--- strIndexable after override ---\n' + strIndexable);
}
exports.usefulToStringDemo = usefulToStringDemo;
//# sourceMappingURL=indexer.js.map