"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Point2DImpl = /** @class */ (function () {
    function Point2DImpl(x, y) {
        this.x = x;
        this.y = y;
    }
    return Point2DImpl;
}());
var Point3DImpl = /** @class */ (function (_super) {
    __extends(Point3DImpl, _super);
    function Point3DImpl(x, y, z) {
        var _this = _super.call(this, x, y) || this;
        _this.z = z;
        return _this;
    }
    return Point3DImpl;
}(Point2DImpl));
var PointDemo = /** @class */ (function () {
    function PointDemo() {
        var point2d = new Point2DImpl(12, 99);
        var point3d = new Point3DImpl(12, 55, 100);
        // point2d.x
        // point3d.z
        var dummyPoint2d = { x: 12, y: 22 };
        var dummyPoint = dummyPoint2d;
        var anotherDummyPoint = { x: 12, y: 34, z: 10 };
        var downcastedPoint = anotherDummyPoint;
        var anotherDowncastedPoint = anotherDummyPoint;
        // downcastedPoint.z
        // anotherDowncastedPoint.z
    }
    return PointDemo;
}());
exports.PointDemo = PointDemo;
//# sourceMappingURL=point.js.map