var MyReadonlyArrayImpl = /** @class */ (function () {
    function MyReadonlyArrayImpl(originalArray) {
        this.backupArray = originalArray.slice();
        this.length = this.backupArray.length;
    }
    MyReadonlyArrayImpl.prototype.get = function (index) {
        return this.backupArray[index];
    };
    return MyReadonlyArrayImpl;
}());
function MyReadonlyArrayDemo() {
    var originalArray = [2, 6, 4, 675, 44.5, 6, 7, 99.56, 0];
    var readonlyArray = new MyReadonlyArrayImpl(originalArray);
    // readonlyArray.length = 12;
    var obj = { length: 12, get: function (index) { return null; } };
    // obj.length = 398;
}
//# sourceMappingURL=readonly.js.map