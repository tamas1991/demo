class MyClass {

    public static creatorFn(): MyClass {
        const instance = new MyClass();
        instance.numField = 23;
        instance.strField = 'str';
        // instance.readonlyField = {};     // Error
        return instance;
    }

    private strField: string;
    public numField: number;
    /*public*/ defaultVisibilityField: string;
    readonly readonlyField: any;

    private constructor() {
        this.readonlyField = {isReadonly: true};
    }

    defaultPublicMethod(param: string): Function {
        // return 23;       // Error
        return function () {
            console.log('returned function called');
        };
    }

    public returnFunctionWithDetailedTypes(): (param1: string, param2: number) => void {
        return (param_1: string, param_2: number) => {
            console.log('param_1: ' + param_1 + '  param_2: ' + JSON.stringify(param_2));
        }

        // Így is lehet:
        // return function (param_1: string, param_2: number) {
        //     console.log('param_1: ' + param_1 + '  param_2: ' + JSON.stringify(param_2));
        // }
    }

}

function myFunction(): void {
    // const myClassInstance = new MyClass();       // Error
    const myClassInstance = MyClass.creatorFn();
    const typedFn = myClassInstance.returnFunctionWithDetailedTypes();
    // typedFn('12', '25');     // Error
    typedFn('12', 25);

    // return 12;       // Error
}