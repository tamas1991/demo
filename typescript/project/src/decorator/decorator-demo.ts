export class DecoratorTests {

    public publicField: string = 'initial value, before everything';

    private privateField: number = 22;
    
    constructor() {
    }

    private traceLogTest(param1, param2): void {
        console.log(`--- trace log test, param1: ${param1}, param2: ${param2}`);
    }

    private protectionTest(): void {
        console.log('--- protected method called ---');
    }

    private testBoth(): void {
        console.log('--- method with both decorators called ---');
    }

    private factoryTest(): void {
        console.log('--- factory test method called ---');
    }

    private methodWithParams(param1: string, param2: number, param3: any): void {
        console.log('methodWithParams called');
    }

}

export class SubClass extends DecoratorTests {

    constructor() {
        super();
        console.log('subclass created');
    }

}

