let instanceCounter = 0;

export function CountInstances(constructorFn: any) {
    return <any>(class extends constructorFn {
        constructor() {
            super(arguments);
            console.log(`Created instances: ${++instanceCounter}`);
        }
    });
}

export function Component(configObj: {selector: string, templateUrl: string, styleUrls?: string[]}) {
    return function(constructorFn: any) {
        return <any>(class extends constructorFn implements AngularComponent {
            public readonly componentMetadata: ComponentConfig;
            constructor() {
                super(arguments);
                this.componentMetadata = ComponentConfig.fromRawObject(configObj);
            }
        });
    }
}

export class ComponentConfig {

    public static fromRawObject(rawObject: {selector: string, templateUrl: string, styleUrls?: string[]}): ComponentConfig {
        return new ComponentConfig(rawObject.selector, rawObject.templateUrl, rawObject.styleUrls);
    }

    constructor(public readonly selector: string,
                public readonly templateUrl: string,
                public readonly styleUrls?: string[]) {
    }

}

export interface AngularComponent {

    readonly componentMetadata: ComponentConfig;

}
