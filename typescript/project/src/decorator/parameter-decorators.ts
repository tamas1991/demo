function getMetadataFieldNameFor(methodName: string): string {
    return `_required_argument_indices_for_${methodName}`;
}

export function Required(target: any, key: string, index: number): void {
    const metadataFieldName = getMetadataFieldNameFor(key);
    if (!target[metadataFieldName]) {
        target[metadataFieldName] = [];
    }
    const requiredIndices: number[] = target[metadataFieldName];
    requiredIndices.push(index);
}

export function ValidateArgs(target: any, key: string, descriptor: PropertyDescriptor): void {
    const metadataFieldName = getMetadataFieldNameFor(key);
    const requiredIndices = target[metadataFieldName];
    if (!requiredIndices) {
        return;
    }

    const originalMethod = descriptor.value;
    descriptor.value = function() {
        const originalArgs = arguments;
        requiredIndices.forEach(requiredArgIndex => {
            const argValue = originalArgs[requiredArgIndex];
            if (typeof argValue === 'undefined' || argValue === null) {
                throw new Error(`You gave "${argValue}" to method "${key}" in place of a required parameter! `);
            }
        });
        originalMethod(arguments);
    }

}