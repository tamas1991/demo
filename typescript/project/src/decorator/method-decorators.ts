import { StaticUtils } from "./static-utils";

/**
 * Puts a log before and after a function call
 * @param target Either the constructor function of the class for a static member or the prototype of the class for an instance member.
 * @param key The name of the member.
 * @param descriptor The Property Descriptor for the member.
 */
export function TraceLog(target: any, key: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function() {
        console.log(`Start method: ${key}`);
        originalMethod.apply(this, arguments);
        console.log(`End method: ${key}`);
    }
}

export function Protected(target: any, key: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function() {
        if (!StaticUtils.authenticated) {
            console.error('You must be authenticated to use this method!');
            return;
        }
        originalMethod.apply(this, arguments);
    }
}

export function DecoratorFactory(configObject: {protection?: boolean, trace?: boolean}) {
    return (target: any, key: string, descriptor: PropertyDescriptor) => {
        if (configObject.protection) {
            this.Protected(target, key, descriptor);
        } if (configObject.trace) {
            this.TraceLog(target, key, descriptor);
        }
    }
}

export function Override(objectPrototype: any, methodName: string, descriptor: PropertyDescriptor): void {
    let superClassPrototype = objectPrototype.__proto__;
    let superMethodFound = false;
    while (Object.keys(superClassPrototype).length > 0) {       // While not empty object
        if (typeof superClassPrototype[methodName] !== 'undefined' && superClassPrototype[methodName] !== null) {       // Maszek isNullOrUndefined
            superMethodFound = true;
            break;
        } 
        superClassPrototype = superClassPrototype.__proto__;
    }

    if (!superMethodFound) {
        throw new Error(`The method "${methodName}" is marked by @Override but it doesn't actually override a superclass method!`);
    }

}

