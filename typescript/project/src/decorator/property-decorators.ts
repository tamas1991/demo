export function LogChanges(target: Object, key: string) {

    // original value
    let value = target[key];

    // getter and setter
    const getter = () => value;
    const setter = (newValue: any) => {
        value = newValue;
        console.log(`new value of ${key}: ${newValue}`);
    };

    // replace property with the getter and setter
    if (delete target[key]) {
        Object.defineProperty(target, key, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
    }

}