import { Component, CountInstances } from "./class-decorators";
import { DecoratorFactory, Protected, TraceLog, Override } from "./method-decorators";
import { LogChanges } from "./property-decorators";
import { Required, ValidateArgs } from "./parameter-decorators";

class VerySuper {

    // public overrideTest(): void {
    // }

    public onlyVerySuperMethod(): void {

    }

}

class SomeOtherClass extends VerySuper {

    // public overrideTest(): void {
    // }

}

// @CountInstances
// @Component({
//     selector: 'app-my-component',
//     templateUrl: './my-template.html',
//     styleUrls: ['./my-style1.scss', './my-style2.scss']
// })
export class DecoratorTests extends SomeOtherClass {

    // @LogChanges
    public publicField: string = 'initial value, before everything';

    // @LogChanges
    private privateField: number = 22;
    
    constructor() {
        super();
        // this.publicField = 'first value, in constructor';
        // this.privateField = 5987;
        // this.traceLogTest('param-1', 12);

        // console.log('-- before auth --');
        // this.protectionTest();
        // this.testBoth();
        // StaticUtils.authenticated = true;
        // console.log('-- after auth --');
        // this.protectionTest();
        // this.testBoth();

        // this.factoryTest();
        // this.methodWithParams('first param', 33, {id: 10, name: 'Name'});
        // setTimeout(() => {
            // Ha csak this -t logolom ki, akkor ezeket a metaadatokat nem fogja kiírni!!!
            // Megy timout nélkül is!!!
            console.log('--- this[....] ---\n', this[`_required_argument_indices_for_methodWithParams`]);
        // }, 2000);
        // this.overrideTest();
    }

    @TraceLog
    private traceLogTest(param1, param2): void {
        console.log(`--- trace log test, param1: ${param1}, param2: ${param2}`);
    }

    @Protected
    private protectionTest(): void {
        console.log('--- protected method called ---');
    }

    // Számít a sorrend!!
    @Protected
    @TraceLog
    private testBoth(): void {
        console.log('--- method with both decorators called ---');
    }

    @DecoratorFactory({
        protection: true,
        trace: true
    })
    private factoryTest(): void {
        console.log('--- factory test method called ---');
    }

    @ValidateArgs
    private methodWithParams(@Required param1: string, @Required param2: number, @Required param3: any): void {
        console.log('methodWithParams called');
    }

    @Override
    public overrideTest(): void {

    }

}

export class SubClass extends DecoratorTests {

    constructor() {
        super();
        console.log('subclass created');
    }

}

