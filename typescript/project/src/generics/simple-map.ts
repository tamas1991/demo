export class SimpleMap<K, V> {

    public static createFrom<K, V>(elements: any[], keyField: string): SimpleMap<K, V> {
        const newSimpleMap = new SimpleMap<K, V>();
        elements
            .map(currentElement => ({key: currentElement[keyField], value: currentElement}))
            .forEach(entry => newSimpleMap.put(entry.key, entry.value));
        return newSimpleMap;
    }

    public static logSimpleMap(mapToLog: SimpleMap<any, any>): void {
        mapToLog.entryset().forEach(entry => console.log(`key: ${entry.key}, value: ${JSON.stringify(entry.value)}`));
    }

    private entries: {key: K, value: V}[];

    constructor() {
        this.entries = [];
    }

    public put(key: K, value: V): void {
        const alreadyContainedObject = this.entries.find(entry => entry.key === key);
        if (typeof alreadyContainedObject !== 'undefined' && alreadyContainedObject !== null) {
            alreadyContainedObject.value = value;
        } else {
            this.entries.push({key, value});
        }
    }

    public get(key: K): V {
        const entry = this.entries.find(entry => entry.key === key);
        return typeof entry === 'undefined' || entry === null ? null : entry.value;
    }

    public entryset(): Set<{key: K, value: V}> {
        return new Set<{key: K, value: V}>(this.entries);
    }

}
