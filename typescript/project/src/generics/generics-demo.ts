import { SimpleMap } from "./simple-map";

export class GenericsDemo {

    constructor() {
        this.mapDemo();
    }

    private mapDemo(): void {
        const map = new SimpleMap<string, number[]>();
        map.put('some key 1', [1,9,33,678, 34.567, 33.102]);
        map.put('some key 2', [1,9,]);
        map.put('some key 3', [33.102]);
        map.put('some key 4', []);
        // map.put(32432, [2,5,9]);

        console.log('some key 1: ' + map.get('some key 1'));
        SimpleMap.logSimpleMap(map);

        const objects: Identifiable[] = [
            {id: 1, name: 'Name 1', text: 'Text 1'},
            {id: 2, name: 'Name 2'},
            {id: 3, text: 'Text 1'},
            {id: 4, name: 'Name 4', text: 'Text 1', extra: 'Extra'},
            {id: 5, name: 'Name 5', text: 'Text 1'},
            {id: 6, name: 'Name 6'},
            {id: 7, text: 'Text 1'},
        ];
        const mapFromObjects = SimpleMap.createFrom<number, number>(objects, 'id');
        console.log('------------------------------------');
        SimpleMap.logSimpleMap(mapFromObjects);
    }


}

interface Identifiable {
    id: number;
    [x: string]: any;
}
