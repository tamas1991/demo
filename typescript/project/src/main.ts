import {DecoratorTests} from './decorator/decorator-tests';
import {AngularComponent, ComponentConfig} from './decorator/class-decorators'
import { GenericsDemo } from './generics/generics-demo';
import { usefulToStringDemo } from './interfaces/indexer';
import { unionDemo } from './composed-types.ts/composed-types';
import { moduleTestDemo } from './modules/module-demo';

const decoratorTests = new DecoratorTests();

// const isAngularComponent = (decoratorTests['componentMetadata']) instanceof ComponentConfig;
// if (isAngularComponent) {
//     const angularComponent = (decoratorTests as any) as AngularComponent;
//     const metadata = angularComponent.componentMetadata;
//     console.log('--- angular component, metadata: ---\n', metadata);
// } else {
//     console.log('--- nem angular component ---');
// }

// const genericsDemo = new GenericsDemo();
// usefulToStringDemo();
// unionDemo();
// moduleTestDemo();