export function moduleFn(): void {
    console.log('moduleFn called');
}

function innerFn(): void {

}

export class ModuleClass {
    constructor() {
        console.log('ModuleClass created');
    }
}

class InnerClass {

}

export const CONSTANTS = {CONST1: 'CONST1', CONST2: 'CONST2'};
