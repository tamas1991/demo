import * as importedMembers from './module01';

export function moduleTestDemo(): void {
    console.log('--- importedMembers ---', importedMembers);
    importedMembers.moduleFn();
    new importedMembers.ModuleClass();
}