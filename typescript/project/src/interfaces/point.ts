interface Point2D {

    x: number;
    y: number;

}

interface Point extends Point2D {

    z?: number;

}

class Point2DImpl implements Point {

    constructor(public x: number, public y: number) {
    }

}

class Point3DImpl extends Point2DImpl {

    constructor(x: number, y: number, public z: number) {
        super(x, y);
    }

}

export class PointDemo {

    constructor() {
        const point2d: Point2D = new Point2DImpl(12, 99);
        const point3d: Point = new Point3DImpl(12, 55, 100);
        // point2d.x
        // point3d.z

        const dummyPoint2d: Point2D = {x: 12, y: 22};
        const dummyPoint: Point = dummyPoint2d;
        const anotherDummyPoint: Point = {x: 12, y: 34, z: 10};

        const downcastedPoint = anotherDummyPoint as Point2D;
        const anotherDowncastedPoint = <Point2D>anotherDummyPoint;
        // downcastedPoint.z
        // anotherDowncastedPoint.z
    }

}