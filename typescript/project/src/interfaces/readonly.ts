interface MyReadonlyArray {

    readonly length: number;
    get(index: number): any;

}

class MyReadonlyArrayImpl implements MyReadonlyArray {

    private backupArray: any[];
    public readonly length: number;

    constructor(originalArray: any[]) {
        this.backupArray = [...originalArray];
        this.length = this.backupArray.length;
    }

    get(index: number): any {
        return this.backupArray[index];
    }

}

function MyReadonlyArrayDemo() {
    const originalArray = [2,6,4,675,44.5, 6, 7, 99.56, 0];
    const readonlyArray = new MyReadonlyArrayImpl(originalArray);
    // readonlyArray.length = 12;

    const obj: MyReadonlyArray = {length: 12, get: index => null};
    // obj.length = 398;
    
}

