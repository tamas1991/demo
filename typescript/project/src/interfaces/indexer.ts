interface NumIndexable {
    [index: number]: any;
}

interface StrIndexable {
    [paramName: string]: any;
}

interface BothIndexable {
    [index: number]: any;
    [paramName: string]: any;
}

interface NotIndexable {
    onlyParam: any;
}

function indexableDemo(): void {
    const numIndexable: NumIndexable = {0: 'Value 1', 1: 'Value 2' /*'strKey': 'Value 3*/ };
    const strIndexable: StrIndexable = {'key0': 'Value 1', 'key1': 'Value 2', 3: 'Value 3'};
    const bothIndexable: BothIndexable = {'key0': 'Value 1', 'key1': 'Value 2', 3: 'Value 3'};
    const notIndexable: NotIndexable = {onlyParam: 'value', /*extraParam: 'value 2'*/};
}

export function usefulToStringDemo(): void {
    const strIndexable: StrIndexable = {'key0': 'Value 1', 'key1': 'Value 2', 3: 'Value 3'};
    console.log('--- strIndexable before override ---\n' + strIndexable);

    // Replace the default toString method on Object.prototype
    Object.prototype.toString = function() {return JSON.stringify(this)};
    console.log('--- strIndexable after override ---\n' + strIndexable);
}
