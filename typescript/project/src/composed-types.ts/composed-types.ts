interface Colored {
    color: string;
    common: string;
}

interface Identified {
    id: number;
    common: string;
}

interface Both extends Colored, Identified {
}


function mergeObjects<T, U>(primaryObj: T, secondaryObj: U): T & U {
    const result = {} as T & U;
    Object.assign(result, primaryObj);
    Object.assign(result, secondaryObj);
    return result;
}

function intersectionDemo(): void {
    const colored: Colored = {color: 'blue', common: 'colored common'};
    const identified: Identified = {id: 12, common: 'identified common'};
    const both = mergeObjects(colored, identified);
//    const both: Both = mergeObjects(colored, identified);
    
    both.id;
    both.color;
}

export function unionDemo(): void {
    const obj = {0: 'value 0', 1: 'vlaue 1', 2: 'value 2'};
    const element0 = accessElement(obj, 0);
    const element2 = accessElement(obj, 'aa');
    console.log(`element 0: ${element0}`);
    console.log(`element 2: ${element2}`);
}

function accessElement(obj: Object, key: string | number): any {
    // const index = stringKeyToIndex(key);
    const index = typeof key === 'string' ? stringKeyToIndex(key) : key;
    return obj[index];
}

function stringKeyToIndex(key: string): number {
    let index;
    // some very difficult calculation, decode hash...
    index = key.length;

    return index;
}

