function feladat01() {
	var printFunctions = [];
	for (var i = 0; i < 5; i++) {
		// A ciklus minden körében hozz létre egy function-t, ami kiírja az "i" ciklusváltozó értékét.
		// Ezeket a metódusokat gyűjtsd össze a printFunction tömbbe.
		// A cél, hogy mikor a lentebb lévő ciklus meghívja a függvényeket, akkor 0 -tól 4-ig legyenek kiírva a számok.
		// A már meglévő kódrészek maradjanak úgy, ahogy vannak.
	}
	
	
	for (let fn of printFunctions) {
		fn();
	}
	
}

feladat01();

function feladat02() {
	var printFunctions = [];
	for (var i = 0; i < 5; i++) {
		// A feladat lényege ugyanaz, mint az előzőé.
		// Tehát az előző feladatot hogy lehetne egyszerűbben megoldani,
		// ha feloldjuk azt a kikötést, hogy a már meglévő kódrészek maradjanak úgy ahogy vannak?
	}
	
	
	for (let fn of printFunctions) {
		fn();
	}
		
}

feladat02();