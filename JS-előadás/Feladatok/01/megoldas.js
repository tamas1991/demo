function feladat01() {
	var printFunctions = [];
	for (var i = 0; i < 5; i++) {
		printFunctions.push(
			(function (param) {
				return (function() {console.log(`i: ${param}`);});
			})(i)
		);
	}
	
	
	for (let fn of printFunctions) {
		fn();
	}
	
}

feladat01();

function feladat02() {
	var printFunctions = [];
	for (let i = 0; i < 5; i++) {
		printFunctions.push(function () {console.log(`i: ${i}`);});
	}
	
	
	for (let fn of printFunctions) {
		fn();
	}
		
}

feladat02();