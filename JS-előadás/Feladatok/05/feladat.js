function print() {
	console.log('print method called');
}

// Feladat: Írd át a function-t úgy, hogy 3 másodpercig folyamatosan hívódjon meg a "print" metódus,
// de egyrészt ne fagyassza le a böngésző fület, másrészt 3 másodperc után álljon is le.
function feladat01() {
	let condition = true;
	setTimeout(() => condition = false, 3000);
	while (condition) {
		print();
	}
}

feladat01();