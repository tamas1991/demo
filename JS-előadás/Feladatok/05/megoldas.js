function print() {
	console.log('print method called');
}

function feladat01() {
	intervalId = setInterval(() => print(), 0);
	setTimeout(() => clearInterval(intervalId), 3000);
}

feladat01();
