function getDataObject() {
	
	return {
		containerId: 'main-container',
		labelContent: 'My awesome label'
	};
	
}

function createHtmlPartString() {
	const dataObject = getDataObject();
	return '<div id="' + dataObject.containerId + '">'
			+ '<label>' + dataObject.labelContent + '</label>'
			+ '</div>';
}

function createHtmlPartStringEasily() {
	const dataObject = getDataObject();
	// Feladat: Ez a függvény ugyanazzal a string-el térjen vissza, mint a fentebb lévő "createHtmlPartString".
	// De ne használj string összefűzést.
}

var htmlPart = createHtmlPartString();
console.log('--- összefűzéssel ---\n', htmlPart);
var htmlPartEasily = createHtmlPartStringEasily();
console.log('--- egyszerűbb megoldással ---\n', htmlPartEasily);