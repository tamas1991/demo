function getDataObject() {
	
	return {
		containerId: 'main-container',
		labelContent: 'My awesome label'
	};
	
}

function createHtmlPartString() {
	const dataObject = getDataObject();
	return '<div id="' + dataObject.containerId + '">\n'
			+ '\t<label>' + dataObject.labelContent + '</label>\n'
			+ '</div>';
}

function createHtmlPartStringEasily() {
	const dataObject = getDataObject();
	return`
		<div id="${dataObject.containerId}">
			<label>${dataObject.labelContent}</label>
		</div>
	`;
}

var htmlPart = createHtmlPartString();
console.log('--- összefűzéssel ---\n', htmlPart);
var htmlPartEasily = createHtmlPartStringEasily();
console.log('--- egyszerűbb megoldással ---\n', htmlPartEasily);