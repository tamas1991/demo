class HostClass {
	
	constructor(hostField) {
		this.hostField = hostField;
	}
	
	printHostField() {
		console.log('host field: ' + this.hostField);
	}
	
}

class ConsumetClass {
	
	constructor(hostMethod) {
		this.hostMethod = hostMethod;
	}
	
	callHostMethod() {
		this.hostMethod();
	}
	
}

function feladat01() {
	const hostInstance = new HostClass('Host field content');
	
	let hostMethod;
	// Megoldás 1:
	hostMethod = () => hostInstance.printHostField();
	// Megoldás 2:
	hostMethod = hostInstance.printHostField.bind(hostInstance);
	
	
	const consumerInstance = new ConsumetClass(hostMethod);
	consumerInstance.callHostMethod();
}
