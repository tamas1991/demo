class HostClass {
	
	constructor(hostField) {
		this.hostField = hostField;
	}
	
	printHostField() {
		console.log('host field: ' + this.hostField);
	}
	
}

class ConsumetClass {
	
	constructor(hostMethod) {
		this.hostMethod = hostMethod;
	}
	
	callHostMethod() {
		this.hostMethod();
	}
	
}

function feladat01() {
	const hostInstance = new HostClass('Host field content');
	
	// Feladat: A hostMethod változónak adj értéket úgy, hogy mikor lentebb meghívjuk a
	// consumerInstance callHostMethod függvényét, akkor az alábbi üzenet jelenjen meg: "host field: Host field content"
	let hostMethod;
	
	
	const consumerInstance = new ConsumetClass(hostMethod);
	consumerInstance.callHostMethod();
	
	return hostMethod;		// A 2. feladathoz kell majd
}
