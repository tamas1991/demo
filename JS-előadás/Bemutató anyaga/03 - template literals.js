var multilineString = `Első sor,
Második sor,
Harmadik sor.`;
console.log(multilineString);

var nameObject = {
	first: 'Jocelyn',
	middle: 'Bell',
	last: 'Burnell'
};
var nameString = `First: ${nameObject.first}, middle: ${nameObject.middle}, last: ${nameObject.last},`;
console.log(nameString);

var expressionString = `The result of 2 + 7 + 19 is ${2 + 7 + 19}`;
console.log(expressionString);

// Agyzsibbasztó syntax
function templateStrFn(strings, ...values) {
	console.log('strings: ' + JSON.stringify(strings));
	console.log('values: ' + JSON.stringify(values));
}
templateStrFn(`Template str ${34 * 9} function ${true} called`);
templateStrFn`Template str ${34 * 9} function ${true} called`;			// Minden ugyanaz, mint egy sorral feljebb, csak a zárójelpár hiányzik

