// Deklarálás előtti használat
function varFn() {
    console.log('typeof v: ' + typeof v);
    var v;
}
function letFn() {
    console.log('typeof v: ' + typeof v);
    let v;
}
function constFn() {
    console.log('typeof v: ' + typeof v);
    const v;
}


// Ciklusváltozó
function varFor() {
    const fnArray = [];
    for (var i = 0; i < 5; i++) {
        fnArray[i] = function () {console.log('i: ' + i);};
    }
    
    for (var j = 0; j < 5; j++) {
        fnArray[j]();
    }
}

function letFor() {
    const fnArray = [];
    for (let i = 0; i < 5; i++) {
        fnArray[i] = function () {console.log('i: ' + i);};
    }
    
    for (var j = 0; j < 5; j++) {
        fnArray[j]();
    }
}

function constFor() {
    const fnArray = [];
    for (const i = 0; i < 5; i++) {
        fnArray[i] = function () {console.log('i: ' + i);};
    }
    
    for (var j = 0; j < 5; j++) {
        fnArray[j]();
    }
}

function workingVarFor() {
    const fnArray = [];
    for (var i = 0; i < 5; i++) {
        fnArray[i] = (function (innerI) {
            return function () {console.log('i: ' + innerI);}
        })(i);
    }

    for (var j = 0; j < 5; j++) {
        fnArray[j]();
    }
}



