export class EgyszeruKerelemVo {

    // Alapadatok
    jogcim: string;

    // E-napló adatai
    naploNev: string;
    naploTipus: string;
    sajatTulajdon: string;
    alapterulet: string;
    lakasokSzama: string;
    megjegyzes: string;

    // Építész tervező adatai
    epiteszNuj: string;
    epiteszNevjegyzekiSzam: string;

    // Érték adatok
    szamitottErtek: string;

}