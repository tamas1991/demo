import { Directive, TemplateRef, ViewContainerRef, Input, HostListener, Renderer2, Renderer, Host, ElementRef } from '@angular/core';

@Directive({
  selector: '[tooltipText]'
})
export class TooltipTextDirective {

  @Input() tooltipText: string;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    @Host() host: ElementRef,
    private renderer: Renderer2) {
      console.log('--- host ---\n', templateRef.elementRef);
      renderer.listen(templateRef.elementRef.nativeElement, 'click', () => {
        console.log('logging click');
      });
      viewContainer.createEmbeddedView(templateRef);
    }

    // @HostListener('click')
    // public createTooltip(): void {
    //   console.log('createTooltip');
    // }

    // @HostListener('onclick')
    // public destroyTooltip(): void {
    //   console.log('destroyTooltip');
    // }

}
