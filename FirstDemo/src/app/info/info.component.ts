import { Component, OnInit } from '@angular/core';
import { MockService } from '../service/MockService';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  public tableData: any;

  constructor(private mockService: MockService) {
    this.mockService.getInfoTableData()
      .then(response => {
        this.tableData = response;
      });
  }

  ngOnInit() {
  }

}
