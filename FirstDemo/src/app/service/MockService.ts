export class MockService {

    public getInfoTableData(): Promise<any> {
        const infoTableData = [
            {bruttoFizetendo: '2 620', szamitottErtekFelett: '0', szamitottErtekig: '50 000 000'},
            {bruttoFizetendo: '3 930', szamitottErtekFelett: '50 000 000', szamitottErtekig: '100 000 000'},
            {bruttoFizetendo: '7 860', szamitottErtekFelett: '100 000 000', szamitottErtekig: '250 000 000'},
            {bruttoFizetendo: '11 790', szamitottErtekFelett: '250 000 000', szamitottErtekig: '500 000 000'},
            {bruttoFizetendo: '18 340', szamitottErtekFelett: '500 000 000', szamitottErtekig: '1000 000 000'},
            {bruttoFizetendo: '23 580', szamitottErtekFelett: '1000 000 000', szamitottErtekig: 'minden nagyobb értékre'},      
        ];

        return this.resolveDataAsJsonInPromise(infoTableData);
    }

    public saveEgyszeruKerelem(vo: any, success = true): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            success
                ? resolve({message: 'Sikeres mentés!'})
                : reject(new Error('Nem sikerült a mentés, mert...'));
        });
    }

    private resolveDataAsJsonInPromise(data: any): Promise<any> {
        return new Promise<any>(resolve => {
            const dataJson = JSON.parse(JSON.stringify(data));
            resolve(dataJson);
        });
    }

    public getJogcimOptions(): Promise<any> {
        const jogcimOptions = [
            {label: 'Saját nevében', id: 0},
            {label: 'Természetes személy meghatalmazottja', id: 1},
            {label: 'Jogi személy meghatalmazottja', id: 2},
            {label: 'Több építtető', id: 3},
        ];

        return this.resolveDataAsJsonInPromise(jogcimOptions);
    }

    public getTulajdonviszonyOptions(): Promise<any> {
        const tulajdonviszonyOptions = [
            {elnevezes: 'Igen', value: true},
            {elnevezes: 'Nem', value: false}
        ];

        return this.resolveDataAsJsonInPromise(tulajdonviszonyOptions);
    }

}