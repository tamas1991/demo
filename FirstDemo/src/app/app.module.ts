import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EgyszeruKerelemComponent } from './egyszeru-kerelem/egyszeru-kerelem.component';
import { FormsModule } from '@angular/forms';
import { InfoComponent } from './info/info.component'
import { MockService } from './service/MockService';
import { TooltipTextDirective } from './tooltip/tooltip-text.directive';

@NgModule({
  declarations: [
    AppComponent,
    EgyszeruKerelemComponent,
    InfoComponent,
    TooltipTextDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    MockService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
