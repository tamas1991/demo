import { Component, OnInit } from '@angular/core';
import { EgyszeruKerelemVo } from '../vo/EgyszeruKerelemVo';

@Component({
  selector: 'app-egyszeru-kerelem',
  templateUrl: './egyszeru-kerelem.component.html',
  styleUrls: ['./egyszeru-kerelem.component.scss']
})
export class EgyszeruKerelemComponent implements OnInit {

  public kerelemVo: EgyszeruKerelemVo;

  constructor() {
    this.kerelemVo = new EgyszeruKerelemVo();

  }

  handleOnSave(): void {
    console.log('--- A vo tartalma ---\n', this.kerelemVo);
  }

  handleOnDelete(): void {
    console.log('handleOnDelete');
  }

  handleOnCancel(): void {
    console.log('handleOnCancel');
  }

  ngOnInit() {
  }

}
